
// * Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.

    document.getElementById("swap").onclick = () => {
        temp = document.getElementById("first").value;
        document.getElementById("first").value = document.getElementById("second").value;
        document.getElementById("second").value = temp;
    }

// * Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.

function insertGreenDivs() {
    for (let i = 0; i < 5; i++){
        let div = document.createElement("div");
        div.innerHTML =`Put the mouse over`;
        div.className = "green_div";
        document.getElementById('wrapper1').append(div);
    }
    changeDivsColor();
}

function changeDivsColor() {
    // let greenDivs = document.getElementsByClassName('green_div');
    let greenDivs = document.getElementById('wrapper1').getElementsByTagName('div');
    greenDivs = [...greenDivs];
    greenDivs.forEach(element => {
    element.addEventListener ('mouseover', function() {
        element.className = "red_div";
        element.innerHTML = null;
        });
    });
}

// onclick = function changeDivsColor() {
//     let divs = wrapper1.getElementsByTagName('div');
//     divs = [...divs];
//     divs.forEach(element => {
//         element.className = "red_div";
//     }
//     );
// }


// * Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна
// згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після
// переміщення інформації

function textField() {
    let textarea = document.createElement('textarea');
    document.getElementById('wrapper2').appendChild(textarea);
    textarea.className = "textField";
    textarea.setAttribute('rows', '5')
    
    let button = document.createElement('button');
    button.innerHTML = 'Publish entered text';
    document.getElementById('wrapper2').appendChild(button);
    
    button.addEventListener('click', function() {
      let div = document.createElement('div');
      div.innerHTML = textarea.value;
      document.getElementById('wrapper2').appendChild(div);
      textarea.value = null;
    });
}

// * Створіть картинку та кнопку з назвою "Змінити картинку"
// зробіть так щоб при завантаженні сторінки була картинка
// https://itproger.com/img/courses/1476977240.jpg
// При натисканні на кнопку вперше картинка замінилася на
// https://itproger.com/img/courses/1476977488.jpg
// при другому натисканні щоб картинка замінилася на
// https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png

function changePic() {
    let div = document.createElement('div');
    let image = document.createElement('img');
    let button = document.createElement('button');
    div.classList = 'wrapper3';
    image.src  = 'https://itproger.com/img/courses/1476977240.jpg'
    button.innerHTML = 'Change the picture';
    div.appendChild(image);
    div.appendChild(button);
    document.body.appendChild(div);
    
    let counter = 0;
    button.addEventListener('click', function() {
        counter++;
        image.src = counter == 1 ? 'https://itproger.com/img/courses/1476977488.jpg' : 
        'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png';
        if (counter == 3) {
            counter = 0;
            image.src  = 'https://itproger.com/img/courses/1476977240.jpg'
        }
    }) ;
}

// Створіть на сторінці 10 параграфів і зробіть так, щоб при натисканні на параграф він зникав

function hideParagraph() {
    for (let i = 0; i < 10; i++) {
        let paragraph = document.createElement('p');
        paragraph.innerHTML = 'Paragraph ' + (i+1);
        paragraph.onclick = function() {
          this.style.display = 'none';
        };
        document.getElementsByClassName('wrapper3')[0].appendChild(paragraph);
      }
}


insertGreenDivs();
textField();
changePic();
hideParagraph()